import java.util.Set;
import java.util.TreeSet;

aspect Errors {
	public class ErrorMessage implements Comparable<ErrorMessage> {
		protected final String message;
		protected final int lineNumber;
		public ErrorMessage(String message, int lineNumber) {
			this.message = message;
			this.lineNumber = lineNumber;
		}
		public int compareTo(ErrorMessage other) {
			if (lineNumber == other.lineNumber) {
				return this.message.compareTo(other.message);
			} else {
				return lineNumber - other.lineNumber;
			}
		}
		public String toString() {
			return "Error at line " + lineNumber + ": " + message;
		}
	} 
	protected ErrorMessage ASTNode.error(String message) {
		return new ErrorMessage(message, getLine(getStart()));
	}

	coll Set<ErrorMessage> Program.errors() [new TreeSet<ErrorMessage>()] with add root Program;
}


aspect ErrorContributions {
	Assign contributes error("Type '" + getIdUse().decl().getTypeOf().getType().toString() + "' for variable '"+ getIdUse().getID() + "' does not match the types '" + getExpr().getType().toString() + "'")
		when !checkTypes()
		to Program.errors() for program();

	Decl contributes error("Type '" + getIdDecl().getTypeOf().getType().toString() + "' for variable '"+ getIdDecl().getID() + "' does not match the types '" + getExpr().getType().toString() + "'")
		when !checkTypes()
		to Program.errors() for program();

	Function contributes error("Function have return type " + getTypeOf().getType().toString() + " but infers type " + getType().getType().toString())
		when recommendNewReturnType()
		to Program.errors() for program();

	FunctionUse contributes error("Function '" + getFunctionName().getID() + "' argument types " + getParamArrayList().toString()  + " does not match the declared parameter types: " + getParamArrayList().toString())
		when !checkParams()
		to Program.errors() for program();

}
