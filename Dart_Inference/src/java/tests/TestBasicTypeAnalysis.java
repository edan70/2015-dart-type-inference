package tests;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import java.io.*;

import lang.ast.Program;
import lang.ast.ErrorMessage;

/**
 * Tests AST dumping
 * @author Malte Johansson <tfy11mj2@student.lu.se>
 * @author Mikael <dat12ml1@student.lu.se>
 */
@RunWith(Parameterized.class)
public class TestBasicTypeAnalysis extends AbstractParameterizedTest {
	/**
	 * Directory where test files live
	 */
	private static final String TEST_DIR = "testfiles/BasicTypeAnalysis";

	/**
	 * Construct a new JastAdd test
	 * @param testFile filename of test input file
	 */
	public TestBasicTypeAnalysis(String testFile) {
		super(TEST_DIR, testFile);
	}

	/**
	 * Run the JastAdd test
	 */
	@Test
	public void runTest() throws Exception {
		Program program = (Program) parse(inFile);
		StringBuilder sb = new StringBuilder();
		for(ErrorMessage em : program.errors()) {
			sb.append(em.toString()).append('\n');
		}
		compareOutput(sb.toString(), outFile, expectedFile);
	}

	@SuppressWarnings("javadoc")
	@Parameters(name = "{0}")
	public static Iterable<Object[]> getTests() {
		return getTestParameters(TEST_DIR);
	}
}
