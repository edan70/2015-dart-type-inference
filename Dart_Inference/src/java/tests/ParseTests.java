package tests;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class ParseTests extends AbstractTestSuite {
	public ParseTests() {
		super("testfiles/parser");// where test input files are
	}


	@Test
	public void Assign() {
		testValidSyntax("Assign.lang");
	}

	@Test
	public void Fib() {
		testValidSyntax("fib.lang");
	}

	@Test
	public void  SimpleDart() {
		testValidSyntax("SimpleDart.lang");
	}
	
	@Test
	public void  Shift() {
		testValidSyntax("Shift.lang");
	}

	@Test
	public void fortest() {
		testValidSyntax("for.lang");
	}
}
