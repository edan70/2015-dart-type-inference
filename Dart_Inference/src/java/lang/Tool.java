package lang;

import java.io.*;
import lang.ast.ErrorMessage;
import lang.ast.LangParser;
import lang.ast.LangScanner;
import lang.ast.Program;
import lang.*;

/**
 * Tests AST dumping
 * @author Malte Johansson <tfy11mj2@student.lu.se>
 * @author Mikael <dat12ml1@student.lu.se>
 */

public class Tool  {


	public static void main(String[] args) throws Exception
	{
		if(args.length != 1) {
			throw new Exception("Wrong number of arguments");
		}
		
		File f = new File(args[0]);
		try{
			Program program = (Program) parse(f);
			StringBuilder sb = new StringBuilder();
		
			for(ErrorMessage em : program.errors()) {
				sb.append(em.toString()).append('\n');
			}
		
			System.out.println(sb.toString());
		} catch( Exception e)
		{
			System.out.println(e.toString());
		}
	
	}

	protected static Object parse(File file) throws IOException, Exception {
		LangScanner scanner = new LangScanner(new FileReader(file));
		LangParser parser = new LangParser();
		return parser.parse(scanner);
	}

}