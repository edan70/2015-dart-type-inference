package lang.ast; // The generated scanner will belong to the package lang.ast

import lang.ast.LangParser.Terminals; // The terminals are implicitly defined in the parser
import lang.ast.LangParser.SyntaxError;

%%

// define the signature for the generated scanner
%public
%final
%class LangScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
%}

// macros
WhiteSpace = [ ] | \t | \f | \n | \r
ID = [a-zA-Z]+ [a-zA-Z0-9]*
INTEGER = [0-9]+
DOUBLE =  ( [0-9]+ ("." [0-9]+)? ) | ( [0-9]* "." [0-9]+ )
BOOL = "true"|"false"
CHAR = "'"[a-zA-Z]"'"
Comment = \/\/[^\n\f\r]*|\/\*[^*]*\*\/|\/*[*]+\/
STRING = (\"[^\"]*\")|("'"[^"'"]*"'")


%%

// discard whitespace information
{WhiteSpace}	{ }
{Comment} 	{ }

// token definitions
"+"				{ return sym(Terminals.ADD);}
"-"				{ return sym(Terminals.SUB);}
"*"				{ return sym(Terminals.MUL);}
"/"				{ return sym(Terminals.DIV);}
"%"				{ return sym(Terminals.MOD);}
"="				{ return sym(Terminals.ASSIGN);}
"=="				{ return sym(Terminals.EQEQ);}	
"!="				{ return sym(Terminals.NOTEQ);}
"<="				{ return sym(Terminals.LESSEQ);}
">="				{ return sym(Terminals.GREATEREQ);}
"<"				{ return sym(Terminals.LESS);}
">"				{ return sym(Terminals.GREATER);}
"&&"				{ return sym(Terminals.AND);}
"||"				{ return sym(Terminals.OR);}
"++"				{ return sym(Terminals.INC);}
"--"				{ return sym(Terminals.DEC);}
"<<"				{ return sym(Terminals.LSHIFT);}
">>"				{ return sym(Terminals.RSHIFT);}

"if"				{ return sym(Terminals.IF);}
"else"				{ return sym(Terminals.ELSE);}
"while"				{ return sym(Terminals.WHILE);}
"for"				{ return sym(Terminals.FOR);}
"return"			{ return sym(Terminals.RETURN);}
"class"				{ return sym(Terminals.CLASS);}
"new"				{ return sym(Terminals.NEW);}


"int"				{ return sym(Terminals.INTEGER);}
"double"			{ return sym(Terminals.DOUBLE);}
"bool"				{ return sym(Terminals.BOOL);}
"num"				{ return sym(Terminals.NUM);}
"String"			{ return sym(Terminals.STRING);}
"var"				{ return sym(Terminals.VAR);}
"char"				{ return sym(Terminals.CHAR);}
"dynamic"			{ return sym(Terminals.DYNAMIC);}


"("				{ return sym(Terminals.LPARAN);}
")"				{ return sym(Terminals.RPARAN);}
"{"				{ return sym(Terminals.LBRACE);}
"}"				{ return sym(Terminals.RBRACE);}
";"				{ return sym(Terminals.SEMICOLON);}
","				{ return sym(Terminals.COMMA);}
"."				{ return sym(Terminals.DOT);}
<<EOF>>				{ return sym(Terminals.EOF); }
{INTEGER}			{ return sym(Terminals.INTEGERVALUE); }
{DOUBLE}			{ return sym(Terminals.DOUBLEVALUE); }
{BOOL}				{ return sym(Terminals.BOOLVALUE); }
{ID}				{ return sym(Terminals.ID); }
{CHAR}				{ return sym(Terminals.CHARVALUE); }
{STRING}			{ return sym(Terminals.STRINGVALUE); }


/* error fallback */
[^]				{ throw new SyntaxError("Illegal character <"+yytext()+">"); }
